﻿namespace TripleTriad
{
    partial class menuForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pvpButton = new System.Windows.Forms.Button();
            this.pveButton = new System.Windows.Forms.Button();
            this.helpButton = new System.Windows.Forms.Button();
            this.exitButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // pvpButton
            // 
            this.pvpButton.Location = new System.Drawing.Point(45, 13);
            this.pvpButton.Name = "pvpButton";
            this.pvpButton.Size = new System.Drawing.Size(92, 23);
            this.pvpButton.TabIndex = 0;
            this.pvpButton.Text = "Plaver vs Player";
            this.pvpButton.UseVisualStyleBackColor = true;
            this.pvpButton.Click += new System.EventHandler(this.pvpButton_Click);
            // 
            // pveButton
            // 
            this.pveButton.Location = new System.Drawing.Point(45, 43);
            this.pveButton.Name = "pveButton";
            this.pveButton.Size = new System.Drawing.Size(92, 23);
            this.pveButton.TabIndex = 1;
            this.pveButton.Text = "Player vs Bot";
            this.pveButton.UseVisualStyleBackColor = true;
            this.pveButton.Click += new System.EventHandler(this.pveButton_Click);
            // 
            // helpButton
            // 
            this.helpButton.Location = new System.Drawing.Point(45, 73);
            this.helpButton.Name = "helpButton";
            this.helpButton.Size = new System.Drawing.Size(92, 23);
            this.helpButton.TabIndex = 2;
            this.helpButton.Text = "How to Play";
            this.helpButton.UseVisualStyleBackColor = true;
            // 
            // exitButton
            // 
            this.exitButton.Location = new System.Drawing.Point(45, 103);
            this.exitButton.Name = "exitButton";
            this.exitButton.Size = new System.Drawing.Size(92, 23);
            this.exitButton.TabIndex = 3;
            this.exitButton.Text = "Exit";
            this.exitButton.UseVisualStyleBackColor = true;
            this.exitButton.Click += new System.EventHandler(this.exitButton_Click);
            // 
            // menuForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(193, 140);
            this.Controls.Add(this.exitButton);
            this.Controls.Add(this.helpButton);
            this.Controls.Add(this.pveButton);
            this.Controls.Add(this.pvpButton);
            this.Name = "menuForm";
            this.Text = "Start Menu";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button pvpButton;
        private System.Windows.Forms.Button pveButton;
        private System.Windows.Forms.Button helpButton;
        private System.Windows.Forms.Button exitButton;
    }
}

