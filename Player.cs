﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TripleTriad
{
    class Player
    {
        public Card[] hand;
        public Player()
        {
            
        }

        public void drawHand()
        {
            for (int i = 0; i < 5; i++)
                hand[i] = new Card();
        }
    }
    class Human : Player
    {
        public Human()
        {
            hand = new Card[5];
            drawHand();
        }
    }
    class Bot : Player
    {
        public Bot()
        {
            hand = new Card[5];
            drawHand();
        }

    }
}
