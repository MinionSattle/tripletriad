﻿namespace TripleTriad
{
    partial class tableForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.fieldPb = new System.Windows.Forms.PictureBox();
            this.playerOneCardOnePb = new System.Windows.Forms.PictureBox();
            this.playerOneCardTwoPb = new System.Windows.Forms.PictureBox();
            this.playerOneCardThreePb = new System.Windows.Forms.PictureBox();
            this.playerOneCardFourPb = new System.Windows.Forms.PictureBox();
            this.playerOneCardFivePb = new System.Windows.Forms.PictureBox();
            this.playerTwoCardFivePb = new System.Windows.Forms.PictureBox();
            this.playerTwoCardFourPb = new System.Windows.Forms.PictureBox();
            this.playerTwoCardThreePb = new System.Windows.Forms.PictureBox();
            this.playerTwoCardTwoPb = new System.Windows.Forms.PictureBox();
            this.playerTwoCardOnePb = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.fieldPb)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.playerOneCardOnePb)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.playerOneCardTwoPb)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.playerOneCardThreePb)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.playerOneCardFourPb)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.playerOneCardFivePb)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.playerTwoCardFivePb)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.playerTwoCardFourPb)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.playerTwoCardThreePb)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.playerTwoCardTwoPb)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.playerTwoCardOnePb)).BeginInit();
            this.SuspendLayout();
            // 
            // fieldPb
            // 
            this.fieldPb.Location = new System.Drawing.Point(98, 12);
            this.fieldPb.Name = "fieldPb";
            this.fieldPb.Size = new System.Drawing.Size(424, 424);
            this.fieldPb.TabIndex = 0;
            this.fieldPb.TabStop = false;
            // 
            // playerOneCardOnePb
            // 
            this.playerOneCardOnePb.Location = new System.Drawing.Point(12, 12);
            this.playerOneCardOnePb.Name = "playerOneCardOnePb";
            this.playerOneCardOnePb.Size = new System.Drawing.Size(80, 80);
            this.playerOneCardOnePb.TabIndex = 1;
            this.playerOneCardOnePb.TabStop = false;
            // 
            // playerOneCardTwoPb
            // 
            this.playerOneCardTwoPb.Location = new System.Drawing.Point(12, 98);
            this.playerOneCardTwoPb.Name = "playerOneCardTwoPb";
            this.playerOneCardTwoPb.Size = new System.Drawing.Size(80, 80);
            this.playerOneCardTwoPb.TabIndex = 2;
            this.playerOneCardTwoPb.TabStop = false;
            // 
            // playerOneCardThreePb
            // 
            this.playerOneCardThreePb.Location = new System.Drawing.Point(12, 184);
            this.playerOneCardThreePb.Name = "playerOneCardThreePb";
            this.playerOneCardThreePb.Size = new System.Drawing.Size(80, 80);
            this.playerOneCardThreePb.TabIndex = 3;
            this.playerOneCardThreePb.TabStop = false;
            // 
            // playerOneCardFourPb
            // 
            this.playerOneCardFourPb.Location = new System.Drawing.Point(12, 270);
            this.playerOneCardFourPb.Name = "playerOneCardFourPb";
            this.playerOneCardFourPb.Size = new System.Drawing.Size(80, 80);
            this.playerOneCardFourPb.TabIndex = 4;
            this.playerOneCardFourPb.TabStop = false;
            // 
            // playerOneCardFivePb
            // 
            this.playerOneCardFivePb.Location = new System.Drawing.Point(12, 356);
            this.playerOneCardFivePb.Name = "playerOneCardFivePb";
            this.playerOneCardFivePb.Size = new System.Drawing.Size(80, 80);
            this.playerOneCardFivePb.TabIndex = 5;
            this.playerOneCardFivePb.TabStop = false;
            // 
            // playerTwoCardFivePb
            // 
            this.playerTwoCardFivePb.Location = new System.Drawing.Point(528, 356);
            this.playerTwoCardFivePb.Name = "playerTwoCardFivePb";
            this.playerTwoCardFivePb.Size = new System.Drawing.Size(80, 80);
            this.playerTwoCardFivePb.TabIndex = 10;
            this.playerTwoCardFivePb.TabStop = false;
            // 
            // playerTwoCardFourPb
            // 
            this.playerTwoCardFourPb.Location = new System.Drawing.Point(528, 270);
            this.playerTwoCardFourPb.Name = "playerTwoCardFourPb";
            this.playerTwoCardFourPb.Size = new System.Drawing.Size(80, 80);
            this.playerTwoCardFourPb.TabIndex = 9;
            this.playerTwoCardFourPb.TabStop = false;
            // 
            // playerTwoCardThreePb
            // 
            this.playerTwoCardThreePb.Location = new System.Drawing.Point(528, 184);
            this.playerTwoCardThreePb.Name = "playerTwoCardThreePb";
            this.playerTwoCardThreePb.Size = new System.Drawing.Size(80, 80);
            this.playerTwoCardThreePb.TabIndex = 8;
            this.playerTwoCardThreePb.TabStop = false;
            // 
            // playerTwoCardTwoPb
            // 
            this.playerTwoCardTwoPb.Location = new System.Drawing.Point(528, 98);
            this.playerTwoCardTwoPb.Name = "playerTwoCardTwoPb";
            this.playerTwoCardTwoPb.Size = new System.Drawing.Size(80, 80);
            this.playerTwoCardTwoPb.TabIndex = 7;
            this.playerTwoCardTwoPb.TabStop = false;
            // 
            // playerTwoCardOnePb
            // 
            this.playerTwoCardOnePb.Location = new System.Drawing.Point(528, 12);
            this.playerTwoCardOnePb.Name = "playerTwoCardOnePb";
            this.playerTwoCardOnePb.Size = new System.Drawing.Size(80, 80);
            this.playerTwoCardOnePb.TabIndex = 6;
            this.playerTwoCardOnePb.TabStop = false;
            // 
            // tableForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(622, 447);
            this.Controls.Add(this.playerTwoCardFivePb);
            this.Controls.Add(this.playerTwoCardFourPb);
            this.Controls.Add(this.playerTwoCardThreePb);
            this.Controls.Add(this.playerTwoCardTwoPb);
            this.Controls.Add(this.playerTwoCardOnePb);
            this.Controls.Add(this.playerOneCardFivePb);
            this.Controls.Add(this.playerOneCardFourPb);
            this.Controls.Add(this.playerOneCardThreePb);
            this.Controls.Add(this.playerOneCardTwoPb);
            this.Controls.Add(this.playerOneCardOnePb);
            this.Controls.Add(this.fieldPb);
            this.Name = "tableForm";
            this.Text = "Tripe Triad";
            ((System.ComponentModel.ISupportInitialize)(this.fieldPb)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.playerOneCardOnePb)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.playerOneCardTwoPb)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.playerOneCardThreePb)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.playerOneCardFourPb)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.playerOneCardFivePb)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.playerTwoCardFivePb)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.playerTwoCardFourPb)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.playerTwoCardThreePb)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.playerTwoCardTwoPb)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.playerTwoCardOnePb)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox fieldPb;
        private System.Windows.Forms.PictureBox playerOneCardOnePb;
        private System.Windows.Forms.PictureBox playerOneCardTwoPb;
        private System.Windows.Forms.PictureBox playerOneCardThreePb;
        private System.Windows.Forms.PictureBox playerOneCardFourPb;
        private System.Windows.Forms.PictureBox playerOneCardFivePb;
        private System.Windows.Forms.PictureBox playerTwoCardFivePb;
        private System.Windows.Forms.PictureBox playerTwoCardFourPb;
        private System.Windows.Forms.PictureBox playerTwoCardThreePb;
        private System.Windows.Forms.PictureBox playerTwoCardTwoPb;
        private System.Windows.Forms.PictureBox playerTwoCardOnePb;
    }
}