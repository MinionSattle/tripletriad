﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TripleTriad
{
    class Card
    {
        public int[] values;
        public Card()
        {
            values = new int[4];
            Random rng = new Random();
            for (int i = 0; i < 4; i++)
                values[i] = rng.Next(0, 10);
        }
    }
}
