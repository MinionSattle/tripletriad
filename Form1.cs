﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TripleTriad
{
    public partial class menuForm : Form
    {
        public menuForm()
        {
            InitializeComponent();
        }

        private void exitButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void pvpButton_Click(object sender, EventArgs e)
        {
            play(true);
        }

        private void pveButton_Click(object sender, EventArgs e)
        {
            play(false);
        }

        private void play(Boolean player)
        {
            Form playForm = new tableForm(player);
            playForm.Show();
            this.Close();
        }
    }
}
