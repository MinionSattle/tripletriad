﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TripleTriad
{
    public partial class tableForm : Form
    {
        Graphics fieldPaper;
        Graphics[] player1Hand, player2Hand;
        Graphics[][] playerHands;
        Player[] players;
        public tableForm(Boolean player)
        {
            InitializeComponent();
            fieldPaper = fieldPb.CreateGraphics();
            players = new Player[2];
            if (player)
                players[0] = new Human();
            else
                players[0] = new Bot();
            player1Hand = new Graphics[5];
            player2Hand = new Graphics[5];

            playerHands = new Graphics[2][];

            player1Hand[0] = playerOneCardOnePb.CreateGraphics();
            player1Hand[1] = playerOneCardTwoPb.CreateGraphics();
            player1Hand[2] = playerOneCardThreePb.CreateGraphics();
            player1Hand[3] = playerOneCardFourPb.CreateGraphics();
            player1Hand[4] = playerOneCardFivePb.CreateGraphics();

            player2Hand[0] = playerTwoCardOnePb.CreateGraphics();
            player2Hand[1] = playerTwoCardTwoPb.CreateGraphics();
            player2Hand[2] = playerTwoCardThreePb.CreateGraphics();
            player2Hand[3] = playerTwoCardFourPb.CreateGraphics();
            player2Hand[4] = playerTwoCardFivePb.CreateGraphics();

            playerHands[0] = player1Hand;
            playerHands[1] = player2Hand;
            drawHands();
        }

        private void drawHands()
        {
            for(int p = 0; p < 2; p++)
            {
                for(int i = 0;i< 5; i++)
                {
                    for(int c = 0; c < 4; c++)
                    {
                        Point location = new Point(0,0);
                        switch(c){
                            case 0: 
                                location = new Point(0, playerOneCardOnePb.Height/2);
                                break;
                            case 1:
                                location = new Point(playerOneCardOnePb.Width/2, 0);
                                break;
                            case 2:
                                location = new Point(playerOneCardOnePb.Width, playerOneCardOnePb.Height/2);
                                break;
                            case 3:
                                location = new Point(playerOneCardOnePb.Width/2, playerOneCardOnePb.Height);
                                break;
                            default:
                                break;
                        }
                        Image value;
                        switch (players[p].hand[i].values[c])
                        {
                            case 0:
                                value = Properties.Resources.A;
                                break;
                            case 1:
                                value = Properties.Resources._1;
                                break;
                            case 2:
                                value = Properties.Resources._2;
                                break;
                            case 3:
                                value = Properties.Resources._3;
                                break;
                            case 4:
                                value = Properties.Resources._4;
                                break;
                            case 5:
                                value = Properties.Resources._5;
                                break;
                            case 6:
                                value = Properties.Resources._6;
                                break;
                            case 7:
                                value = Properties.Resources._7;
                                break;
                            case 8:
                                value = Properties.Resources._8;
                                break;
                            case 9:
                                value = Properties.Resources._9;
                                break;
                            default:
                                value = Properties.Resources._1;
                                break;
                        }
                        playerHands[p][i].DrawImage(value, location);
                    }
                }
            }
        }
    }
}
